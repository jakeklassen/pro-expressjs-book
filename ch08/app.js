// Pro Express.js Chapter 08 - Response Object

var fs = require('fs');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// Some Chapter Routes for Testing

app.get('/render', function (req, res) {
  // No parameters
  res.render('render');
});

app.get('/render-title', function (req, res) {
  // Pass in some template data
  // Use callback to log the template HTML returned by .render()
  res.render('index', { title: 'Pro Express.js '}, function renderCallback (error, html) {
    if (error) return next(error);

    // Log HTML data
    console.log(html);
  });
});

// Use `locals` to pass data
app.get('/locals', function (req, res) {
	// No changes to index.jade needed
	res.locals = { title: 'Pro Express.js - Response.locals' };
	res.render('index');
});

// It may not be immediately clear what `response.locals` is good for.
// The advantage is that we can expose (i.e., pass to templates) info in one middleware,
// but render the actual template later in another request handler. For example, you can 
// perform authentication without rendering.

/*
app.get('/locals', function (req, res) {
	res.locals = { user: {admin: true}};
	next();
}, function (req, res) {
	res.render('index');
});
*/

// This route will dump the `response.locals` value into the node app log.
app.get('/debug', function (req, res) {
	res.locals = {
		message: 'See node application console',
		title: 'Pro Express.js - debug route'
	};

	res.render('debug');
});

// Use response.set() to set html headers
app.get('/set-html', function (req, res) {
	res.set('Content-Type', 'text/html');
	res.end(`
	<html>
		<body>
			<h1>Express.js - Setting Headers</h1>
		</body>
	</html>
	`);
});

// Send a csv file with multiple headers set - will download the file.
app.get('/set-csv', function(req, res) {
	var body = 'title, tags\n' +
		'Practical Node.js, node.js express.js\n' +
		'Rapid Prototyping with JS, backbone.js node.js mongodb\n' +
		'JavaScript: The Good Parts, javascript\n';

	res.set({
		'Content-Type': 'text/csv',
		'Content-Length': body.length,
		'Set-Cookie': ['type=reader', 'language=javascript']
	});
	
	res.end(body);
});

// Use status() to set the proper status code for a route.
app.get('/status', function (req, res) {
	res.status(200).end();
});

// Combine status code with data using proper header set by .send()
app.get('/send-ok', function (req, res) {
	res.status(200).send({
		message: 'Data was submitted successfully'
	});
});

// Send a 500 error
app.get('/send-err', function (req, res) {
	res.status(500).send({
		message: 'Oops, the server is down'
	});
});

// Override content-type by .send() when sending text in a buffer.
app.get('/send-buf', function (req, res) {
	res.set('Content-Type', 'text/plain');
	res.send(new Buffer('text data that will be converted into Buffer'));
});

// Send JSON
app.get('/json', function(req, res) {
	res.status(200).json([{title: 'Practical Node.js', tags: 'node.js express.js'},
		{title: 'Rapid Prototyping with JS', tags: 'backbone.js node.js mongodb'},
		{title: 'JavaScript: The Good Parts', tags: 'javascript'}
	]);
});

// Send jsonp response
app.get('/jsonp', function (req, res) {
	res.status(200).jsonp([
		{title: 'Express.js Guide', tags: 'node.js express.js'},
		{title: 'Rapid Prototyping with JS', tags: 'backbone.js, node.js, mongodb'},
		{title: 'JavaScript: The Good Parts', tags: 'javascript'}
	]);
});

// Redirect to google
app.get('/redirect-to-google', function (req, res) {
	res.redirect('http://google.com');
});

// Use streams (non-blocking) for a large image
app.get('/stream', function (req, res) {
	var stream = fs.createReadStream(path.join(__dirname, 'public', 'images', 'deep-space.jpg'));
	stream.pipe(res);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
