var cluster = require('cluster');
var http = require('http');
var numCPUS = require('os').cpus().length;
var app = require('./app');

if (cluster.isMaster) {
	console.log(`Fork ${numCPUS} from master`);

	for (var i = 0; i < numCPUS; ++i) {
		cluster.fork();
	}

	cluster.on('online', (worker) => {
		console.log(`Worker is running on ${worker.process.pid} pid`);
	});

	cluster.on('exit', (worker, code, signal) => {
		console.log(`Worker with ${worker.process.pid} is closed`);
	});
} else if (cluster.isWorker) {
	const port = 3000;
	console.log(`Worker ${cluster.worker.process.pid} is now listening to http://localhost:${port}`);

	app.listen(port);
}