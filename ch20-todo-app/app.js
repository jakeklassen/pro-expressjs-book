var express = require('express');
var http = require('http');
var path = require('path');
var mongoskin = require('mongoskin');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('express-session');
var csrf = require('csurf');
var errorHandler = require('errorhandler');

var routes = require('./routes/index');
var tasks = require('./routes/tasks');

// Connect to the db
var db = mongoskin.db('mongodb://localhost:27017/todo?auto_reconnect', {safe:true});

var app = express();

// Export database object to middleware for route access
app.use(function (req, res, next) {
  req.db = { };
  req.db.tasks = db.collection('tasks');
  next();
});

app.locals.appname = 'Express.js Todo App';
app.set('port', process.env.PORT || 3000);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride());
// Shold be an ENV variable
app.use(cookieParser('CEAF3FA4-F385-49AA-8FE4-54766A9874F1'));
app.use(session({
  // Should be an ENV variable
  secret: '59B93087-78BC-4EB9-993A-A61FC844F6C9',
  resave: true,
  saveUninitialized: true
}));

app.use(csrf());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// csrf middleware
app.use(function (req, res, next) {
  res.locals._csrf = req.csrfToken();
  return next();
});

// If url contains task_id, attach to request
app.param('task_id', function (req, res, next, taskId) {
  req.db.tasks.findById(taskId, function (error, task) {
    if (error) return next(error);
    if (!task) return next(new Error('Task not found'));

    req.task = task;
    next();
  })
});

app.use('/', routes.index);
app.get('/tasks', tasks.list);
// Mark all tasks as completed
app.post('/tasks', tasks.markAllCompleted);
// Add new task
app.post('/tasks', tasks.add);
// Mark single task as completed
app.post('/tasks/:task_id', tasks.markCompleted);
app.delete('/tasks/:task_id', tasks.del);
app.get('/tasks/completed', tasks.completed);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(errorHandler());
  // app.use(function(err, req, res, next) {
  //   res.status(err.status || 500);
  //   res.render('error', {
  //     message: err.message,
  //     error: err
  //   });
  // });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

http.createServer(app).listen(app.get('port'), function () {
  console.log(`Express server listening on port ${app.get('port')}`);
});
