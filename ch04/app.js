var express = require('express');
var path = require('path');
var fs = require('fs');
var favicon = require('serve-favicon');
var timeout = require('connect-timeout');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
// Allows developers to process incoming data, such as body payload, into usable
// JavaScript/Node.js objects.
var bodyParser = require('body-parser');
var compression = require('compression');
var csurf = require('csurf');
// Enables support for HTTP methods that might be unsupported by clients —
// for example, systems where requests are limited to GET and POST
// (such as an HTML form in the browser).
var methodOverride = require('method-override');
var serveIndex = require('serve-index');
var busboy = require('connect-busboy');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// enable gzip compression
// threshold - the size in kilobits that can go uncompressed
app.use(compression({ threshold: 1 }));

app.use(methodOverride());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// Process JSON data and store in req.body
// MIME type application/json
app.use(bodyParser.json());
// Process URL-encoded (querystring) data; e.g., ?name=value&name2=value2
// and store the values in req.body
app.use(bodyParser.urlencoded({ extended: false }));
// Data accessible in req.cookie
app.use(cookieParser());

// Must be after cookieParser
// Look into `resave` with redis store
app.use(session({
  resave: true,
  saveUninitialized: false,
  secret: 'a magical strong to prevent tampering jerks'
}));

// Enable CSRF protection
// The csrf middleware must be after express-session, cookie-parser, and
// optionally (meaning if you plan to support tokens in the body of requests)
// after body-parser middlewares
// app.use(csurf());

// Serve shared files as a directory listing
app.use('/shared', serveIndex(path.join('public', 'shared'), { icons: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// Allow uploads using busboy
app.use('/upload', busboy({ immediate: true }));
app.use('/upload', function (req, res) {
  req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
    file.on('data', function (data) {
      fs.writeFile('uploads/' + fieldname + filename, data);
    });

    file.on('end', function () {
      console.log('File ' + filename + ' is ended');
    });
  });

  req.busboy.on('finish', function () {
    console.log('Busboy is finished');
    res.status(201).end();
  });
});

// Example route to showcase connect-timeout
// 50/50 chance that no timeout will be issued. If a timeout occurs the client
// will receive a 503 - service unavailable.
app.get(
  '/slow-request',
  timeout('1s'),
  function slowRequest(req, res, next) {
    setTimeout(function () {
      if (req.timeout) return false;

      return next();
    }, 999 + Math.round(Math.random()));
  },
  function slowRequestSuccess(req, res, next) {
    res.send('ok');
  }
);

/*
Testing the GET /purchase-orders route will give you a JSON object containing a
"csrfToken" value. Here is how you could test the GET and DELETE routes for this
address using httpie:

```bash
// Start and store a session
> http --session=user1 http://localhost:3000
{
  "csrfToken": "SomeLongTokenId"
}

> http --session=user1 DELETE http://localhost:3000 _csrf=SomeLongTokenId
```
*/

// NOT IN A REAL APP PLZ
app.get('/purchase-orders', function (req, res) {
  res.send({ csrfToken: req.csrfToken() });
});

// Using method-override we can have routes like this
app.delete('/purchase-orders', function(req, res){
  console.log('The DELETE route has been triggered');
  res.status(204).end();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    if (err.code === 'EBADCSRFTOKEN') {
      console.log(req.body);
      console.log('Bad CSRF Token');
    }

    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
