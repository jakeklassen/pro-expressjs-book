var express = require('express')
	, app = express()
	, port = 3000;


// Wildcard route - regex
app.get('*', function requestHandler(request, response) {
	response.end('Hello World');
});

app.listen(port, function appListen () {
	console.log(`The server is running, please open your browser at http://localhost:${port}`);
});