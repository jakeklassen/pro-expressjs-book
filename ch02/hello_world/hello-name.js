var express = require('express')
	, app = express()
	, port = 3000;

// Order matters! If the wildcard route was first, we'd never reach
// this route.
app.get('/name/:user_name', function helloName (req, res) {
	res.status(200);
	res.set('Content-type', 'text/html');
	// res.send() automatically adds Content-Length header
	res.send(`
<html>
	<body>
		<h1>Hello ${req.params.user_name}</h1>
	</body>
</html>`);
});

// Wildcard route - regex
app.get('*', function requestHandler(request, response) {
	response.end('Hello World');
});

app.listen(port, function appListen () {
	console.log(`The server is running, please open your browser at http://localhost:${port}`);
});