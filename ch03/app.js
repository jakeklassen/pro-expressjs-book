var book = {
	name: 'Practical Node.js',
	publisher: 'Apress',
	keywords: 'node.js express.js mongodb websocket oauth',
	discount: 'PNJS15'
};

var express = require('express')
	, path = require('path');

var app = express();

console.log(app.get('env'));

//---------------//
// Configuration //
//---------------//

// Good for production
app.set('view cache', true);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);

app.set('trust proxy', true);
app.set('jsonp callback name', 'cb');
app.set('json replacer', function jsonReplacer (key, value) {
	if (key === 'discount') {
		return undefined;
	} else {
		return value;
	}
});
app.set('json spaces', 4);

// users/ and Users/ are distinct routes
app.set('case sensitive routing', true);
// users and users/ are distinct routes
app.set('strict routing', true);
// Let's not tell what framework we use
app.set('x-powered-by', false);
app.set('subdomain offset', 3);
// app.disable('etag');

//------------//
// Middleware //
//------------//

app.get('/jsonp', function (req, res) {
	res.jsonp(book);
});

app.get('/json', function (req, res) {
	res.send(book);
});

app.get('/users', function (req, res) {
	res.send('users');
});

app.get('/users/', function (req, res) {
	res.send('/users/');
});

// Example for error
app.get('/error', function (req, res, next) {
	next(new Error("Here's your error"));
})

app.get('*', function (req, res) {
	res.send('Pro Express.js Configurations');
});

// Send stack traces
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);

		// Pass along some view data
		res.render('error', {
			message: err.message,
			status: err.status || 500,
			error: err
		});
	});
}

var server = app.listen(app.get('port'), function () {
	console.log(`Express server listening on port ${server.address().port}`);
});