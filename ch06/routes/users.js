var express = require('express');
var router = express.Router();

// Our awesome database
var users = {
  1: {
    name: 'Jake',
    age: 28,
    dob: '11/20/1986',
    role: 'developer'
  }
};

router.param('id', function (req, res, next, id) {
  if (undefined === users[id]) {
    return next(new Error('User not found'));
  }

  req.user = users[id];
  next();
});

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(users);
});

router.get('/:id', function (req, res) {
  res.send(req.user);
});

module.exports = router;
