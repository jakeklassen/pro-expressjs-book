var express = require('express');
var superagent = require('superagent');
var consolidate = require('consolidate');
var path = require('path');

var app = express();

// Config //

app.engine('html', consolidate.handlebars);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));

var user = 'azat_co';
var story_slug = 'kazan';

// Storify //

var api_key = '';
var username = '';
var _token = '';

// Routes //

app.get('/', function (req, res, next) {
  superagent.get(`http://api.storify.com/v1/stories/${user}/${story_slug}`)
    .query({
      api_key: api_key,
      username: username,
      _token: _token
    })
    .set({ Accept: 'application/json' })
    .end(function (err, storifyResponse) {
      if (err) return next(err);

      return res.render('index', storifyResponse.body.content);
    });
});

app.listen(3001, function () {
  console.log('server listening');
});