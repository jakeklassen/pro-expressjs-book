// Pro Express.js Chapter 07 - Request Object

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// request.query
// The query string is anything after the `?` in the URI
app.get('/search', function (req, res) {
  console.log(req.query);

  res.end(JSON.stringify(req.query) + '\r\n');
});

// request.params
// :role, :name, :status will be accssible as an object in request.params
app.get('/params/:role/:name/:status', function (req, res) {
  // Output current routes information provided by request.route
  console.log(req.route);
  console.log(req.params);
  res.end();
});

// route to showcase body-parser data in request.body
app.post('/body', function (req, res) {
  console.log(req.body);
  res.end(JSON.stringify(req.body) + '\r\n');
});

// Working with cookies
app.get('/cookies', function (req, res) {
  if (!req.cookies.counter) {
    res.cookie('counter', 0);
  } else {
    res.cookie('counter', parseInt(req.cookies.counter, 10) + 1);
  }

  res.status(200).send(req.cookies);
});

// Show request header data by name
app.get('/header', function (req, res) {
  console.log('Content-Type is ', req.header('content-type'));
  res.end();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
