Code from [Pro Express.js](http://www.amazon.com/gp/product/1484200381?keywords=pro%20express.js&qid=1444688889&ref_=sr_1_1&s=books&sr=1-1) reading.

# Tips

`app.get('env')` is a convenient method for `process.env.NODE_ENV;` in other words, the preceding line can be rewritten with `process.env.NODE_ENV === 'development'`. 